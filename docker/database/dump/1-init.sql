CREATE DATABASE mvc_php;
CREATE DATABASE mvc_php_test;

CREATE USER 'user'@'%' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON mvc_php.* TO 'user'@'%';
GRANT ALL PRIVILEGES ON mvc_php_test.* TO 'user'@'%';

FLUSH PRIVILEGES;
