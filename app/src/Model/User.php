<?php
namespace App\Model;

use Carbon\Carbon;
class User
{
    protected int $id;
    protected string $usuario;
    protected string $email;
    protected string $password; // Lembre-se de aplicar hash à senha
    protected string $nome;
    protected ?string $nascimento; // Pode ser null se não fornecido
    protected string $createdAt;
    protected string $updatedAt;

    // Construtor com propriedades tipadas
    public function __construct(
        string  $usuario = '',
        string  $email = '',
        string  $password = '',
        string  $nome = '',
        ?string $nascimento = null
    )
    {
        $this->usuario = $usuario;
        $this->email = $email;
        $this->password = $password;
        $this->nome = $nome;
        $this->nascimento = $nascimento;
    }

    // Getters e Setters
    public function getId(): int
    {
        return $this->id;
    }

    public function getUsuario(): string
    {
        return $this->usuario;
    }

    public function setUsuario(string $usuario): void
    {
        $this->usuario = $usuario;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email):void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setNome(string $nome): void
    {
        $this->nome = trim($nome);
    }

    public function getNascimento(): ?string
    {
        return $this->nascimento;
    }

    public function setNascimento(string $nascimento): void
    {
        $this->nascimento = $nascimento;
    }

    public function getCreatedAt(): string
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->createdAt)
            ->format('d/m/Y H:i:s');
    }

    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): string
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->updatedAt)
            ->format('d/m/Y H:i:s');
    }

    public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}