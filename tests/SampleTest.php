<?php
use PHPUnit\Framework\TestCase;

class SampleTest extends TestCase
{
    public function simulate($input1, $input2, $input3, $input4)
    {
        return ($input1 && ($input2 && $input3) || $input4);
    }

    public function testTrueAssertsToTrue()
    {
        $this->assertTrue($this->simulate(true, false, false, true));
    }
}
