<?php
namespace Factory;

use PDO;
use PHPUnit\Framework\TestCase;

use App\Factory\DatabaseFactory;


class DatabaseFactoryTest extends TestCase
{
    public function testDatabaseConnection(): void
    {
        // Chama o método estático create da classe DatabaseFactory
        $pdo = DatabaseFactory::create();

        // Verifica se o retorno é uma instância de PDO
        $this->assertInstanceOf(PDO::class, $pdo);
    }
}