<?php

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ApiRoutesTest extends TestCase
{
    protected $client;

    protected function setUp(): void
    {
        $this->client = new Client([
            'base_uri' => 'http://localhost:8080', // Ajuste para a URL da sua API
            'http_errors' => false, // Desativa exceções para códigos de erro HTTP
            'headers' => [
                'Content-Type' => 'application/json', // Definir o Content-Type para application/json
            ],
        ]);
    }
    /**
     * Testar a rota GET /users para listar todos os usuários
     *
     * @test
     * @return void
     */
    public function get_users_route(): void
    {
        // Simular uma requisição GET para /users
        $response = $this->client->request('GET', '/users');

        // Assertivas para verificar se a resposta é a esperada
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($response->getBody());
    }

    /**
     * Testar a rota GET /users/{id} para obter detalhes de um usuário específico
     *
     * @test
     * @return void
     */
    public function get_user_details_route(): void
    {
        // Simular uma requisição GET para /users/1
        $response = $this->client->request('GET', '/users/1');

        // Assertivas para verificar se a resposta é a esperada
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($response->getBody());
    }

    /**
     * Testar a rota POST /users para criar um novo usuário
     *
     * @test
     * @return void
     */
    public function create_user_route(): void
    {
        // Simular uma requisição POST para /users
        $response = $this->client->request('POST', '/users');

        // Assertivas para verificar se a resposta é a esperada
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($response->getBody());
    }

    /**
     * Testar a rota PUT /users/{id} para atualizar um usuário
     *
     * @test
     * @return void
     */
    public function update_user_route(): void
    {
        // Simular uma requisição PUT para /users/1
        $response = $this->client->request('PUT', '/users/1');

        // Assertivas para verificar se a resposta é a esperada
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($response->getBody());
    }

    /**
     * Testar a rota DELETE /users/{id} para remover um usuário
     *
     * @test
     * @return void
     */
    public function delete_user_route(): void
    {
        // Simular uma requisição DELETE para /users/1
        $response = $this->client->request('DELETE', '/users/1');

        // Assertivas para verificar se a resposta é a esperada
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($response->getBody());
    }
}
