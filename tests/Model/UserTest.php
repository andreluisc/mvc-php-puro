<?php
namespace Model;

use App\Model\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * Testa a capacidade de definir e obter o nome de usuário.
     *
     * @test
     * @return void
     */
    public function set_and_get_usuario(): void
    {
        $user = new User();
        $user->setUsuario('johndoe');
        $this->assertEquals('johndoe', $user->getUsuario());
    }

    /**
     * Testa a capacidade de definir e obter o email.
     *
     * @test
     * @return void
     */
    public function set_and_get_email(): void
    {
        $user = new User();
        $user->setEmail('johndoe@example.com');
        $this->assertEquals('johndoe@example.com', $user->getEmail());
    }

    /**
     * Testa a capacidade de definir e obter a senha.
     *
     * @test
     * @return void
     */
    public function set_and_get_password(): void
    {
        $user = new User();
        $user->setPassword('securepassword');
        $this->assertEquals('securepassword', $user->getPassword());
    }

    /**
     * Testa a capacidade de definir e obter o nome.
     *
     * @test
     * @return void
     */
    public function set_and_get_nome(): void
    {
        $user = new User();
        $user->setNome('John Doe');
        $this->assertEquals('John Doe', $user->getNome());
    }

    /**
     * Testa a capacidade de definir e obter a data de nascimento.
     *
     * @test
     * @return void
     */
    public function set_and_get_nascimento(): void
    {
        $user = new User();
        $user->setNascimento('2000-01-01');
        $this->assertEquals('2000-01-01', $user->getNascimento());
    }

    /**
     * Testa o formato da data de criação.
     *
     * @test
     * @return void
     */
    public function format_created_at(): void
    {
        $user = new User();
        $user->setCreatedAt('2020-01-01 00:00:00');
        $this->assertEquals('01/01/2020 00:00:00', $user->getCreatedAt());
    }

    /**
     * Testa o formato da data de atualização.
     *
     * @test
     * @return void
     */
    public function format_updated_at(): void
    {
        $user = new User();
        $user->setUpdatedAt('2020-01-02 00:00:00');
        $this->assertEquals('02/01/2020 00:00:00', $user->getUpdatedAt());
    }
}
