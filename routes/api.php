<?php

// Configuração CORS
header('Access-Control-Allow-Origin: *'); // Permite requisições de qualquer origem. Ajuste conforme necessário.
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS'); // Métodos HTTP permitidos
header('Access-Control-Allow-Headers: Content-Type, Authorization'); // Cabeçalhos permitidos nas requisições

// Opção para tratar requisições preflight do CORS
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    // Retorna apenas os cabeçalhos necessários para o preflight
    exit(0);
}

// Verificação do Content-Type
if ($_SERVER['CONTENT_TYPE'] !== 'application/json') {
    header('HTTP/1.1 400 Bad Request');
    echo json_encode(['error' => 'Invalid Content-Type']);
    exit;
}

// Verificação dos Métodos HTTP
$validMethods = ['GET', 'POST', 'PUT', 'DELETE'];
if (!in_array($_SERVER['REQUEST_METHOD'], $validMethods)) {
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Method Not Allowed']);
    exit;
}

// Lógica de roteamento
$requestUri = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
    case 'GET':
        if ($requestUri == '/users') {
            // Listar todos os usuários
            return ['status' => 200, 'data' => 'Listagem de todos os usuários realizada com sucesso.'];
        } elseif (preg_match('/^\/users\/(\d+)$/', $requestUri, $matches)) {
            $userId = $matches[1];
            // Detalhes do usuário $userId
            return ['status' => 200, 'data' => "Detalhes do usuário $userId obtidos com sucesso."];
        }
        break;
    case 'POST':
        if ($requestUri == '/users') {
            // Criar um novo usuário
            return ['status' => 200, 'data' => 'Novo usuário criado com sucesso.'];
        }
        break;
    case 'PUT':
        if (preg_match('/^\/users\/(\d+)$/', $requestUri, $matches)) {
            $userId = $matches[1];
            // Atualizar o usuário $userId
            return ['status' => 200, 'data' => "Usuário $userId atualizado com sucesso."];
        }
        break;
    case 'DELETE':
        if (preg_match('/^\/users\/(\d+)$/', $requestUri, $matches)) {
            $userId = $matches[1];
            // Remover o usuário $userId
            return ['status' => 200, 'data' => "Usuário $userId removido com sucesso."];
        }
        break;
    default:
        return ['status' => 404, 'data' => '404 Not Found'];
        break;
}